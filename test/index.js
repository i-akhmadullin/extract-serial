var detector = require('../');
var assert = require('assert');

var torrents = [
    'How I Met Your Mother S09E23-E24 HDTV x264-EXCELLENCE [eztv]',
    'The Blacklist S01E18 HDTV x264-LOL [eztv]',
    'How.I.Met.Your.Mother.S09E23-24.Last.Forever.WEB-DL.x264.AAC'
]
var expectedSerialNames = [
    'How I Met Your Mother',
    'The Blacklist',
    'How I Met Your Mother'
]

describe('Detector', function(){
    it('Should parse one serial from TB torrent', function(done){
        assert.deepEqual(detector.detect(torrents[0]), expectedSerialNames[0]);
        done();
    });
    it('Should parse array of serials from TB torrent', function(done){
        assert.deepEqual(detector.detect(torrents), expectedSerialNames);
        done();
    });
});
